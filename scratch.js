function createNewUser() {
    let newUser = {};
    newUser.firstName = prompt("Введіть ім'я:");
    newUser.lastName = prompt("Введіть прізвище:");
    newUser.birthday = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");

    newUser.getLogin = function () {
        return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
    };

    newUser.getAge = function () {
        const today = new Date();
        const birthDate = new Date(newUser.birthday);
        let age = today.getFullYear() - birthDate.getFullYear();
        const monthDifference = today.getMonth() - birthDate.getMonth();

        if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;
    };

    newUser.getPassword = function () {
        const firstInitial = newUser.firstName.charAt(0).toUpperCase();
        const lowercaseLastName = newUser.lastName.toLowerCase();
        const birthYear = newUser.birthday.split(".")[2];

        return firstInitial + lowercaseLastName + birthYear;
    };

    return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
